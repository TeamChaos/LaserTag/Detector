
#ifndef DETECTOR_LIB_CONFIG_H
#define DETECTOR_LIB_CONFIG_H

//#define DEBUG_BLINK


#define RECEIVERS_AT_PA 0b11111000
#define RECEIVERS_AT_PB 0b01111001

#define RECEIVER_BYTES_TO1(a, b) ( ((b >>3) | (a <<1)) & 0x7F ) //Ensure that the MSB is not 1 to avoid 0xFF (I2C_ERROR_BYTE)
#define RECEIVER_BYTES_TO2(a, b) ( (a >> 6) | ((b &0b1))<<2)


#define RECEIVER_BYTE_ONE (~PINA & RECEIVERS_AT_PA)
#define RECEIVER_BYTE_TWO (~PINB & RECEIVERS_AT_PB)


#endif //DETECTOR_LIB_CONFIG_H
