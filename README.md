# Detector Control Code

This code controls the AVR microcontroller handling the IR receive or hit detection.

The exact microcontroller used here is the MicroChip Attiny 861.
A 261/461 would be fine as well. They have 2K/4K Flash and 128/256 Bytes of SRAM respectively.

## AVR Configuration
The microcontroller has to run with full 8 MHz without clock divider. It is also recommend to enable brown-out detection.  
Recommended fuse settings:  
``` -U lfuse:w:0xe2:m -U hfuse:w:0xdd:m -U efuse:w:0xff:m  ```

If you use a different device or frequency you will have to adjust the CMakeList.txt:
```
SET(DEVICE "attiny861")
SET(FREQ "8000000")
add_definitions(-DATTINY861)
```

## Function
This device has to
- listen to numerous receiver modules (about 10)
- decode any received signal independently
- verify and classify decoded signals (which sensor regions were hit)
- report hits to the master module (ESP) via I2C

## Pins
- 18 (SCL): I2C clock
- 20 (SDA): I2C data
- 19 (PA1): Data available (for I2C)
- 4 (PB3): IRin 1
- 7 (PB4): IRin 2
- 8 (PB5): IRin 3
- 9 (PB6): IRin 4
- 11 (PA3): IRin 5
- 12 (PA4): IRin 6
- 13 (PA5): IRin 7
- 14 (PA6): IRin 8
- 17 (PA7): IRin 9


## Code
### Program structure
To decode the signals received from the IR receivers a state machine (one per receiver) is used.
The used states are:
- IDLE
- MARK
- SPACE
- STOP
- OVERFLOW

TIMER1 of the AVR to create interrupt every 50us.
Within the interupt service routine the receiver pin is checked and the state machine is updated appropriately.
Once a state machine has reached the STOP state (after receiving some input and then a long gap), the main loop attempts to decode it.
The ISR fills a buffer/array with the raw timings of spaces and marks. The values represent duration in number of ticks, so the actual duration could be calculated with "value * 50u".
If the number of spaces/marks matches the expected (the weapon always transmits the same number of bits), the timings are matched against the defined durations (including a tolerance).

For the I2C protocol see CommonLib/detector_control/protocol.h


### Development Setup
To work on this project you need the several packages:
avr-binutils, avr-gcc, and avr-libc
Don't forget about git of course.

A CMake compatible IDE is recommended.
E.g. CLion or KDevelop (free)
QtCreator is supposed to work as well