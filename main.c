#define F_CPU 8000000U

#include <version.h>
#include "CommonLib/avr_libs/timers.h"
#include "CommonLib/ir/receive.h"
#include "CommonLib/avr_libs/TWI_slave.h"
#include "CommonLib/detector_control/protocol.h"
#include "config.h"

const char gitversion[8] = GIT_COMMIT_HASH;



/**
 * ###### VARIABLES ######
 */

/**
 * Detector status, see detector_control/protocol.h STATUS_*
 */
volatile uint8_t detector_status;


/**
 * Stores decoded player flag (f) and id (i) and the byte parity bit (p)
 * 0bp00fiiii
 * Only valid if transmit==true
 */
uint8_t data_id;

/**
 * Stores decoded data(d) and the byte parity bit (p)
 * 0bp00ddddd
 *
 * Only valid if transmit==true
 */
uint8_t data_data;

/**
 * One bit corresponds to one receiver. 1 means it received a message
 *
 * LSB: Receiver 1, 2nd-MSB: Receiver 7, MSB: 0
 */
uint8_t status_receiver1;

/**
 * One bit corresponds to one receiver. 1 means it received a message
 *
 * LSB: Receiver 8
 */
uint8_t status_receiver2;


/**
 * Count parity and other errors
 * Store these here for debugging. Can be retrieved via I2C
 */
uint8_t errors = 0;

volatile uint8_t i2c_errors = 0;

volatile bool transmission_complete = false;


/***
 * ####### Predeclare Functions ########
 */



/**
 * Called when a i2c transmission is received
 * @param addr "Address" to write
 * @param data
 */
void i2cReceiveEvent(uint8_t addr, uint8_t data);


/**
 * Called when a i2c transmission request is received
 * @param addr Address to "read"
 * @return
 */
uint8_t i2cRequestEvent(uint8_t addr);

/*
 * Updates detector_status, toggle transmit pin and disable timer interrupts
 */
void inline startTransmission();

/**
 * Update status, toggle transmit pin and enable timer interrupts
 */
void inline finishTransmission();

void inline onI2CError();
void inline onError();


/**
 * ####### MAIN Code #######
 */


void i2cReceiveEvent(uint8_t addr, uint8_t data) {
    if(addr == I2C_STATUS_SET){
        if(data == STATUS_TRANSMIT || data> STATUS_TRANSMIT){ //Cannot be forced into transmit mode. Cannot set non existing mode
            onI2CError();
        }
        else{
            detector_status = data;
            if (data == STATUS_DETECTING) {
                TX_IND_PORT &= ~(1 << TX_IND);
                reset();//Reset receive state machine
                ENABLE_IR_IN_INTR;
            } else {
                TX_IND_PORT &= ~(1 << TX_IND);
                DISABLE_IR_IN_INTR;
            }
        }
    }
    else{
        onI2CError();
    }
}

uint8_t i2cRequestEvent(uint8_t addr) {
    if (detector_status == STATUS_TRANSMIT) {
        switch (addr) {
            case I2C_GET_ALL:
                return I2C_ALL_OK;
            case I2C_ID_GET:
                return data_id;
            case I2C_DATA_GET:
                return data_data;
            case I2C_D_STATUS_ONE_GET:
                return status_receiver1;
            case I2C_D_STATUS_TWO_GET:
                return status_receiver2;
            case I2C_GET_ALL2:
                transmission_complete=true;
                return I2C_ALL_OK;
            default:
                break;
        }
    }
    switch (addr) {
        case I2C_STATUS_GET:
            return detector_status;
        case I2C_ERRORS_GET:
            return errors & 0x7Fu;//Ensure we are not transmitting 0xFF (I2C_ERROR_BYTE)
        case I2C_ERRORS_I2C_GET:
            return i2c_errors & 0x7Fu;//Ensure we are not transmitting 0xFF (I2C_ERROR_BYTE)
        case I2C_READ_VERSION1:
        case I2C_READ_VERSION2:
        case I2C_READ_VERSION3:
        case I2C_READ_VERSION4:
        case I2C_READ_VERSION5:
        case I2C_READ_VERSION6:
        case I2C_READ_VERSION7:
        case I2C_READ_VERSION8:
            return (uint8_t) gitversion[addr - I2C_READ_VERSION1];
        default:
            onI2CError();
            return I2C_ERROR_BYTE;
    }
}


void setup() {

    //Configure as inputs

    DDRA &= ~(RECEIVERS_AT_PA);
    DDRB &= ~(RECEIVERS_AT_PB);

    //Enable pull up, so not connected pins act like inactive receiver
    PORTA |= RECEIVERS_AT_PA;
    PORTB |= RECEIVERS_AT_PB;

    //TX Indicator
    TX_IND_DDR |= (1 << TX_IND);


    i2c_init(DETECTOR_TWI_ADDRESS);
    i2c_onReceivePtr = i2cReceiveEvent;
    i2c_onRequestPtr = i2cRequestEvent;

    setupIRIn();
    detector_status = STATUS_PAUSED;

}


void loop() {

    if (detector_status == STATUS_DETECTING) {

        decode_results results;
        uint8_t result = decode(&results);

        if (result) { //If stopped (result !=null)
            if (result == 1) {
                if (results.parity_ok) {
                    DISABLE_IR_IN_INTR;

                    //Prepare transmission

                    //Encode data to transmit
                    data_id = results.id;
                    if (results.player) {
                        data_id |= 0b10000;
                    }
                    data_id |= (__builtin_parity(data_id) << 7);


                    data_data = results.data;
                    data_data |= (__builtin_parity(data_data) << 7);

                    status_receiver1 = RECEIVER_BYTES_TO1 (active_pins_one, active_pins_two);
                    status_receiver2 = RECEIVER_BYTES_TO2(active_pins_one, active_pins_two);

                    //Signalise transmission ready
                    startTransmission();

                } else {
                    //Parity error
                    onError();
                }
            } else {
                //Overflow or decoding issue
                onError();
            }
            reset();//Reset state machine on error if signal is received. (However, if a signal successfully decoded the Timer interrupts are disabled.
        }
    }
    else{
        if(transmission_complete){
            transmission_complete=false;
            finishTransmission();
        }

    }





}

void inline onError(){
    if(errors != 0xFE){
        errors++;
    }
}

void inline onI2CError(){
    if(i2c_errors != 0xFE){
        i2c_errors++;
    }
}

void inline startTransmission() {
    detector_status = STATUS_TRANSMIT;

    TX_IND_PORT |= (1 << TX_IND);
}

void inline finishTransmission() {
    TX_IND_PORT &= ~(1 << TX_IND);
    detector_status = STATUS_DETECTING;
    //reset(); Already resetting after signal decoded
    ENABLE_IR_IN_INTR;
}


int main(void) {
    initTimers();

    delay(100);

    setup();


    while (1) {
        loop();
    }
    return 0;
}



//DEBUG Methods

/*
void debug_printValues(decode_results results){
    for(int i=0;i<5;i++){
        if(results.data & (1 << i)){
            PORTA |= (1 << PA1);  //Set output off
            delay(1000);
            PORTA &= ~(1 << PA1);  //Set output off
            delay(500);
        }
        else{
            PORTA |= (1 << PA2);  //Set output off
            delay(1000);
            PORTA &= ~(1 << PA2);  //Set output off
            delay(500);
        }
    }
    PORTA |= (1 << PA0);  //Set output off
    delay(1000);
    PORTA &= ~(1 << PA0);  //Set output off
    delay(100);
    for(int i=0;i<4;i++){
        if(results.id & (1 << i)){
            PORTA |= (1 << PA1);  //Set output off
            delay(1000);
            PORTA &= ~(1 << PA1);  //Set output off
            delay(500);
        }
        else{
            PORTA |= (1 << PA2);  //Set output off
            delay(1000);
            PORTA &= ~(1 << PA2);  //Set output off
            delay(500);
        }
    }
    PORTA |= (1 << PA0);  //Set output off
    delay(1000);
    PORTA &= ~(1 << PA0);  //Set output off
    delay(100);

    if(results.player){
        PORTA |= (1 << PA1);  //Set output off
        delay(1000);
        PORTA &= ~(1 << PA1);  //Set output off
        delay(500);
    }
    else{
        PORTA |= (1 << PA2);  //Set output off
        delay(1000);
        PORTA &= ~(1 << PA2);  //Set output off
        delay(500);
    }
}
*/

